# Les franchouillards

## Helm & K8S

This project runs using helm chart througt a microk8s cluster.

### Postgres

Please see this [repository](https://github.com/bitnami/charts/)

```shell script
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm install pg-franchouillard bitnami/postgresql
```

### API

```shell script
# Install CM
helm install pg-franchouillard-cm pg/

# Install API
helm install franchouillard-api -f api/values.yaml api/

```
