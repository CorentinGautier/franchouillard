DROP TABLE IF EXISTS fr_game CASCADE;
DROP TABLE IF EXISTS fr_card CASCADE;
DROP TABLE IF EXISTS fr_user CASCADE;
DROP TABLE IF EXISTS fr_type CASCADE;

create extension if not exists "uuid-ossp";

CREATE TABLE fr_game
(
    id   UUID default uuid_generate_v4() NOT NULL,
    name      TEXT   NOT NULL,
    description  TEXT   ,
    image  TEXT   ,
    publicate  BOOLEAN   NOT NULL,
    date_create      DATE   NOT NULL,
    date_update   DATE   NOT NULL,
    date_publicate   DATE ,
    id_creator   TEXT   NOT NULL,
    minParticipant TEXT    NOT NUll
);

INSERT INTO fr_game VALUES ('522dace4-4c32-422d-b2d3-a3f3eda3fb7d','jeux 1', 'je suis un jeu 1','image',true,'2020-11-02','2020-11-02','2020-11-02','cff9b714-cb0f-411e-9460-268c56215a08', 3);
INSERT INTO fr_game VALUES ('592ab76c-91ce-46df-a079-731ff72695b4','jeux 2', 'je suis un jeu 2','image',false,'2020-11-02','2020-11-02','2020-11-02','cff9b714-cb0f-411e-9460-268c56215a08', 2);
INSERT INTO fr_game VALUES ('c4dd02f0-b359-4a38-9430-fb16ee7e50fc','jeux 3', 'je suis un jeu 3','image',true,'2020-11-02','2020-11-02','2020-11-02','cff9b714-cb0f-411e-9460-268c56215a08', 2);
INSERT INTO fr_game VALUES ('1002321b-0128-4899-ab7e-dd50d4b53c64','jeux 4', 'je suis un jeu 4','image',true,'2020-11-02','2020-11-02','2020-11-02','f9fc9989-51b4-49ac-adf8-7ef9adc5589d', 2);


CREATE TABLE fr_card
(
    id   UUID default uuid_generate_v4() NOT NULL  PRIMARY KEY,
    name      TEXT   ,
    text1  TEXT  NOT NULL ,
    text2  TEXT,
    type  TEXT,
    date_create   DATE,
    date_update   DATE,
    image  TEXT  ,
    id_game TEXT   NOT NULL,
    id_creator   TEXT   NOT NULL
);

INSERT INTO fr_card VALUES ( 'ab9d6d71-7388-4e2d-9668-c5c2b6d062e8', 'nom', 'Ceux qui ont  1 iphone boivent 2 gorgées','', 'boisson', '2020-11-02', '2020-11-02', 'image','522dace4-4c32-422d-b2d3-a3f3eda3fb7d', 'cff9b714-cb0f-411e-9460-268c56215a08' );
INSERT INTO fr_card VALUES ( 'aab62ecf-6cdf-4158-a72b-01368341ca3e', 'nom', '4 gorgées pour tous le monde sauf ##', 'boisson','',  '2020-11-02', '2020-11-02', 'image', 'c4dd02f0-b359-4a38-9430-fb16ee7e50fc', 'cff9b714-cb0f-411e-9460-268c56215a08' );
INSERT INTO fr_card VALUES ( 'baa25b3f-3cb3-4762-af81-500a287bf189', 'nom', '## donnera 2 gorgée à celui qui arrive à sucer son orteil avec ##','', 'action',  '2020-11-02', '2020-11-02', 'image', '522dace4-4c32-422d-b2d3-a3f3eda3fb7d', 'cff9b714-cb0f-411e-9460-268c56215a08');


CREATE TABLE fr_user
(
    id       UUID default uuid_generate_v4() NOT NULL,
    email      TEXT NOT NULL,
    first_name  TEXT   NOT NULL,
    last_name  TEXT    NOT NULL,
    password   TEXT  
);

INSERT INTO fr_user VALUES ('cff9b714-cb0f-411e-9460-268c56215a08','emailCorentin@gmail.com','Corentin','Gautier','jesuisunmpd');
INSERT INTO fr_user VALUES ('f9fc9989-51b4-49ac-adf8-7ef9adc5589d','emailMarine@gmail.com','Marine','Dupont','jesuisMarine');


CREATE TABLE fr_type
(
    id       UUID default uuid_generate_v4() NOT NULL,
    name      TEXT    NOT NULL
);

INSERT INTO fr_type VALUES ('cc4351f7-2b18-479e-85d3-20d13cefb3c9','Question');
INSERT INTO fr_type VALUES ('24886156-4789-4644-9ad2-7f70bed0f391','Defis');
INSERT INTO fr_type VALUES ('66a92f2e-cc8f-4bbe-8c14-39debc957b7b','Devinette');
INSERT INTO fr_type VALUES ('8f717d33-db17-498c-ac8a-cb6d059d9f50','3 mots');