const logger = require('./logger').getLogger('SERVER');
const { start } = require('./src/server');
global.fetch = require('node-fetch')

start()
  .then(() => {
    logger.info('Server up and running!');
  })
  .catch(error => {
    logger.info('Something went wrong!', error);
    process.exit(1);
  });
