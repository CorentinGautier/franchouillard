// Read .env
process.env.NODE_ENV === 'test' ?
    require('dotenv').config({ path: '.env.test' }) :
    require('dotenv').config();

// Initialize logger
require('./logger').init();

const commonEnv = require('common-env/withLogger')(console);

// Define ENV var if they are not in the .env
// These variable are for the test environment
module.exports = commonEnv.getOrElseAll({
    api: {
        host: 'localhost',
        port: '40000'
    },
    postgresql: {
        host: '',
        port: 5432,
        user: '',
        password: '',
        database: ''
    },
    jwt: ''
});
