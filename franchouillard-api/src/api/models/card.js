const R = require('ramda');
const Boom = require('@hapi/boom');

const db = require('../../database').getDb();
const logger = require('../../../logger').getLogger('SERVER');

const tableUserFields = [
    'id',
    'name',
    'text1',
    'text2',
    'type',
    'date_create',
    'date_update',
    'image',
    'id_game',
    'id_creator'
];
exports.getCardsByGameId = async request => {
    return db
        .query(`select * from "fr_card" where "id_game" = '${request.query.id_game}';`)
        .then(result => {
            return result;
        });
};

exports.getCardById = async request => {
    return await db.fr_card
        .findOne({ id: request.payload.id }, { fields: tableUserFields })
        .then(result => {
            return result;
        });
};
exports.insertCard = async request => {
    return db.fr_card.insert(request.payload).then(result => {
        return result;
    });
};

exports.updateCard = async request => {
    const criteria = {
        id: request.payload.id
    };

    const changes = {
        name: request.payload.name,
        text1: request.payload.text1,
        text2: request.payload.text2,
        type: request.payload.type,
        date_update: request.payload.date_update,
        image: request.payload.image,
        id_game: request.payload.id_game,
        id_creator: request.payload.id_creator
    };

    const options = {
        single: true
    };

    return db.fr_card.update(criteria, changes, options).then(result => {
        return result;
    });
};

exports.deleteCard = async request => {
    return await db.fr_card.destroy({ id: request.query.id }).then(result => {
        return result;
    });
};