const R = require('ramda');
const Boom = require('@hapi/boom');

const db = require('../../database').getDb();
const tableUserFields = ['id', 'username', 'email', 'password', 'games'];

exports.login = async request => {
    const val = db.fr_user.findOne({ username: request.query.username, email: request.query.email }, { fields: tableUserFields });
    if (val) {
        return val;
    } else {
        return false;
    }
};
// exports.addgameToUser = async request => {
//     if (val) {
//         this.updateUser;
//     } else {
//         return false;
//     }
// };

exports.updateUser = async request => {
    let gameValidated = [];
    for (let game of request.password.games) {
        const val = db.fr_card.findOne({ id: game.id }, { fields: tableUserFields });
        if (val) {
            gamesValidated.push(game.id);
        }
    }
    const criteria = {
        id: request.payload.id
    };

    const changes = {
        username: request.payload.username,
        email: request.payload.email,
        password: request.payload.password,
        games: gamesValidated
    };

    const options = {
        single: true
    };

    return db.fr_user.update(criteria, changes, options).then(result => {
        return result;
    });
};
exports.getUsers = async request => {
    return db.query(`select * from "fr_user";`).then(result => {
        return result;
    });
};

exports.getUserById = async request => {
    return await db.fr_user.findOne({ id: request.query.id }, { fields: tableUserFields });
};

exports.createUser = async request => {
    return db.fr_user.insert(request.payload).then(result => {
        return result;
    });
};
exports.deleteUser = async request => {
    db.follower.destroy({ id: request.params.id }).then(result => {});
    return db.fr_user.destroy({ id: request.params.id }).then(result => {
        return result;
    });
};