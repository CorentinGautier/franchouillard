const db = require('../../database').getDb();

exports.getTypes = async request => {
    return db.query(`select * from "fr_type";`).then(result => {
        return result;
    });
};

exports.getTypeById = async request => {
    const tableUserFields = ['id', 'name', 'color', 'email'];
    return await db.fr_type.findOne({ id: request.params.id }, { fields: tableUserFields });
};

exports.insertType = async request => {
    return db.fr_type.insert(request.payload).then(result => {
        return result;
    });
};

exports.updateType = async request => {
    const criteria = {
        id: request.payload.id
    };

    const changes = {
        name: request.payload.name,
        color: request.payload.color
    };

    const options = {
        single: true
    };

    return db.fr_type.update(criteria, changes, options).then(result => {
        return result;
    });
};

exports.deleteType = async request => {
    return await db.fr_type.destroy({ id: request.params.id }).then(result => {
        return result;
    });
};