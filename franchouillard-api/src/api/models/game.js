const R = require('ramda');
const Boom = require('@hapi/boom');

const db = require('../../database').getDb();
const logger = require('../../../logger').getLogger('SERVER');

exports.getGameById = async request => {
    const tableUserFields = [
        'id',
        'name',
        'description',
        'image',
        'publicate',
        'date_create',
        'date_update',
        'date_publicate',
        'id_creator',
        'minParticipant'
    ];
    return await db.fr_game.findOne({ id: request.query.id }, { fields: tableUserFields });
};

exports.updateGameMinParticipant = async request => {
    const criteria = {
        id: request.payload.id
    };
    const changes = {
        minParticipant: request.payload.minParticipant
    };
    const options = {
        single: true
    };

    return db.fr_game.update(criteria, changes, options).then(result => {
        return result;
    });
};

exports.getGames = async request => {
    return db.query(`select * from "fr_game";`).then(result => {
        return result;
    });
};
exports.insertGame = async request => {
    return db.fr_game.insert(request.payload).then(result => {
        return result;
    });
};

exports.updateGame = async request => {
    const criteria = {
        id: request.payload.id
    };

    const changes = {
        name: request.payload.name,
        description: request.payload.description,
        image: request.payload.image,
        publicate: request.payload.publicate,
        date_create: request.payload.date_create,
        date_update: request.payload.date_update,
        date_publicate: request.payload.date_publicate,
        id_creator: request.payload.id_creator,
        minParticipant: request.payload.minParticipant
    };

    const options = {
        single: true
    };

    return db.fr_game.update(criteria, changes, options).then(result => {
        return result;
    });
};

exports.deleteGame = async request => {
    return await db.fr_game.destroy({ id: request.query.id }).then(result => {
        return result;
    });
};