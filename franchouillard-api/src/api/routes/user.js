const Joi = require('@hapi/joi');
const handlers = require('../handlers/user.js');

module.exports = [{
        method: 'GET',
        path: '/api/users/login',
        options: {
            auth: false,
            handler: (request, h) => handlers.login(request, h),
            description: 'Récupération username et email lors de la connexion ',
            tags: ['api'],
            validate: {
                query: Joi.object({
                    username: Joi.string()
                        .required()
                        .description('username'),
                    email: Joi.string()
                        .required()
                        .description('user email')
                })
            }
        }
    },
    {
        method: 'GET',
        path: '/api/users',
        options: {
            auth: false,
            handler: (request, h) => handlers.getUsers(request, h),
            description: 'Lister les utilisateurs pour la recherche',
            tags: ['api']
        }
    },
    {
        method: 'GET',
        path: '/api/user',
        options: {
            auth: false,
            handler: (request, h) => handlers.getUserById(request, h),
            description: 'Récupérer l’ensemble des informations d’un utilisateurs',
            tags: ['api'],
            validate: {
                query: Joi.object({
                    id: Joi.string()
                        .uuid()
                        .required()
                        .description('id')
                })
            }
        }
    },
    {
        method: 'POST',
        path: '/api/users',
        options: {
            auth: false,
            handler: (request, h) => handlers.createUser(request, h),
            description: 'Créer un utilisateur',
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    username: Joi.string()
                        .required()
                        .description('User username'),
                    email: Joi.string()
                        .required()
                        .description('User email'),
                    password: Joi.string().description('User password')
                })
            }
        }
    },
    {
        method: 'POST',
        path: '/api/users/game',
        options: {
            auth: false,
            handler: (request, h) => handlers.addgameToUser(request, h),
            description: 'add game to an user',
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    id: Joi.string()
                        .uuid()
                        .required()
                        .description('games id')
                })
            }
        }
    },

    {
        method: 'PUT',
        path: '/api/users',
        options: {
            auth: false, //desactiver l'authentification avec jwt
            handler: (request, h) => handlers.updateUser(request, h),
            description: 'Modifier un utilisateur',
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    id: Joi.string()
                        .required()
                        .description('User id'),
                    username: Joi.string()
                        .required()
                        .description('User username'),
                    email: Joi.string()
                        .required()
                        .description('User email'),
                    password: Joi.string()
                        .required()
                        .description('User password')
                })
            }
        }
    },
    {
        method: 'DELETE',
        path: '/api/users/{id}',
        options: {
            auth: false, //desactiver l'authentification avec jwt
            handler: (request, h) => handlers.deleteUser(request, h),
            description: 'Supprimer un utilisateur',
            tags: ['api']
        }
    }
];