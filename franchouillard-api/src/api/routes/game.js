const Joi = require('@hapi/joi');

const handlers = require('../handlers/game.js');

module.exports = [{
        method: 'GET',
        path: '/api/gameById',
        options: {
            auth: false,
            handler: (request, h) => handlers.getGameById(request, h),
            description: 'Get 1 game information by id.',
            tags: ['api'],
            validate: {
                query: Joi.object({
                    id: Joi.string()
                        .guid()
                        .required()
                        .description('game id')
                })
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/updateGameMinParticipant',
        options: {
            auth: false,
            handler: (request, h) => handlers.updateGameMinParticipant(request, h),
            description: 'updateGameMinParticipant',
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    id: Joi.string()
                        .required()
                        .guid()
                        .description('Game id'),
                    minParticipant: Joi.number()
                        .required()
                        .description('Game min participant')
                })
            },
            payload: {
                multipart: true
            }
        }
    },
    {
        method: 'GET',
        path: '/api/games',
        options: {
            auth: false,
            handler: (request, h) => handlers.getGames(request, h),
            description: 'Get all games informations',
            tags: ['api']
        }
    },
    {
        method: 'POST',
        path: '/api/addGame',
        options: {
            auth: false,
            handler: (request, h) => handlers.insertGame(request, h),
            description: 'Insert a new game',
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    id: Joi.string()
                        .guid()
                        .required()
                        .description('game id'),
                    name: Joi.string()
                        .required()
                        .description('game name'),
                    description: Joi.string()
                        .required()
                        .description('game description'),
                    publicate: Joi.boolean()
                        .required()
                        .description('game publicated'),
                    date_create: Joi.date()
                        .required()
                        .description('game creation date'),
                    date_update: Joi.date()
                        .required()
                        .description('game update date'),
                    date_publicate: Joi.date()
                        .required()
                        .description('game publicate date'),
                    image: Joi.string()
                        .optional()
                        .description('game image'),
                    minParticipant: Joi.number()
                        .required()
                        .description('game minParticipant'),
                    id_creator: Joi.string()
                        .required()
                        .description('game id_creator id')
                }),
                failAction: async(request, h, err) => {
                    console.error(err);
                    throw err;
                }
            },
            payload: {
                multipart: true
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/updateGame',
        options: {
            auth: false,
            handler: (request, h) => handlers.updateGame(request, h),
            description: 'Update a game',
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    id: Joi.string()
                        .guid()
                        .required()
                        .description('game id'),
                    name: Joi.string()
                        .required()
                        .description('game name'),
                    description: Joi.string()
                        .required()
                        .description('game description'),
                    publicate: Joi.boolean()
                        .required()
                        .description('game publicated'),
                    date_create: Joi.date()
                        .required()
                        .description('game creation date'),
                    date_update: Joi.date()
                        .required()
                        .description('game update date'),
                    date_publicate: Joi.date()
                        .required()
                        .description('game publicate date'),
                    image: Joi.string()
                        .optional()
                        .description('game image'),
                    minParticipant: Joi.number()
                        .required()
                        .description('game minParticipant'),
                    id_creator: Joi.string()
                        .required()
                        .description('game id_creator id')
                }),
                failAction: async(request, h, err) => {
                    console.error(err);
                    throw err;
                }
            },
            payload: {
                multipart: true
            }
        }
    },
    {
        method: 'DELETE',
        path: '/api/deleteGame',
        options: {
            auth: false,
            handler: (request, h) => handlers.deleteGame(request, h),
            description: 'Delete game in database',
            tags: ['api'],
            validate: {
                query: Joi.object({
                    id: Joi.string()
                        .guid()
                        .required()
                        .description('Game id')
                })
            }
        }
    }
];