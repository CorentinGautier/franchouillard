const handlers = require('../handlers/type.js');
const Joi = require('@hapi/joi');

module.exports = [{
        method: 'GET',
        path: '/api/typeById',
        options: {
            auth: false,
            handler: (request, h) => handlers.getTypeById(request, h),
            description: 'Get 1 type information by id.',
            tags: ['api'],
            validate: {
                query: Joi.object({
                    id: Joi.string()
                        .guid()
                        .required()
                        .description('type id')
                })
            }
        }
    },
    {
        method: 'GET',
        path: '/api/types',
        options: {
            auth: false,
            handler: (request, h) => handlers.getTypes(request, h),
            description: 'Get all type informations',
            tags: ['api']
        }
    },
    {
        method: 'POST',
        path: '/api/insertType',
        options: {
            auth: false,
            handler: (request, h) => handlers.insertType(request, h),
            description: 'Insert a new type',
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    id: Joi.string()
                        .guid()
                        .required()
                        .description('type id'),
                    name: Joi.string()
                        .required()
                        .description('type name'),
                    color: Joi.string()
                        .required()
                        .description('type color')
                }),
                failAction: async(request, h, err) => {
                    console.error(err);

                    throw err;
                }
            },
            payload: {
                multipart: true
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/updateType',
        options: {
            auth: false,
            handler: (request, h) => handlers.updateType(request, h),
            description: 'Update a type',
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    id: Joi.string()
                        .guid()
                        .required()
                        .description('type id'),
                    name: Joi.string()
                        .required()
                        .description('type name'),
                    color: Joi.string()
                        .required()
                        .description('type color')
                }),
                failAction: async(request, h, err) => {
                    console.error(err);
                    throw err;
                }
            },
            payload: {
                multipart: true
            }
        }
    },
    {
        method: 'DELETE',
        path: '/api/deleteType',
        options: {
            auth: false,
            handler: (request, h) => handlers.deleteType(request, h),
            description: 'Delete type in database',
            tags: ['api'],
            validate: {
                query: Joi.object({
                    id: Joi.string()
                        .guid()
                        .required()
                        .description('Type id')
                })
            }
        }
    }
];