const Joi = require('@hapi/joi');

const handlers = require('../handlers/card.js');

module.exports = [{
        method: 'GET',
        path: '/api/cardsbyGameId',
        options: {
            auth: false,
            handler: (request, h) => handlers.getCardsByGameId(request, h),
            description: 'Get all card information by game id',
            tags: ['api'],
            validate: {
                query: Joi.object({
                    id_game: Joi.string()
                        .guid()
                        .required()
                        .description('game id')
                })
            }
        }
    },
    {
        method: 'GET',
        path: '/api/cardById',
        options: {
            auth: false,
            handler: (request, h) => handlers.getCardById(request, h),
            description: 'Get 1 card information by card id',
            tags: ['api'],
            validate: {
                query: Joi.object({
                    id: Joi.string()
                        .required()
                        .description('card id')
                })
            }
        }
    },
    {
        method: 'POST',
        path: '/api/addCard',
        options: {
            auth: false,
            handler: (request, h) => handlers.insertCard(request, h),
            description: 'Insert a new card',
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    id: Joi.string()
                        .guid()
                        .description('Card id'),
                    name: Joi.string()
                        .optional()
                        .description('Card name'),
                    text1: Joi.string()
                        .required()
                        .description('Card text1'),
                    text2: Joi.string()
                        .optional()
                        .description('Card text2'),
                    type: Joi.string()
                        .optional()
                        .description('Card type'),
                    date_create: Joi.date()
                        .required()
                        .description('Card creation date'),
                    date_update: Joi.date()
                        .required()
                        .description('Card update date'),
                    image: Joi.string()
                        .optional()
                        .description('Card image'),
                    id_creator: Joi.string()
                        .required()
                        .description('Card id_creator id'),
                    id_game: Joi.string()
                        .guid()
                        .required()
                        .description("Games card's id")
                }).allow(null),
                failAction: async(request, h, err) => {
                    console.error(err);
                    throw err;
                }
            },
            payload: {
                multipart: true
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/updateCard',
        options: {
            auth: false,
            handler: (request, h) => handlers.updateCard(request, h),
            description: 'Update a card',
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    id: Joi.string()
                        .guid()
                        .required()
                        .description('Card id'),
                    name: Joi.string()
                        .optional()
                        .description('Card name'),
                    text1: Joi.string()
                        .required()
                        .description('Card text1'),
                    text2: Joi.string()
                        .optional()
                        .description('Card text2'),
                    type: Joi.string()
                        .optional()
                        .description('Card type'),
                    date_create: Joi.date()
                        .required()
                        .description('Card creation date'),
                    date_update: Joi.date()
                        .required()
                        .description('Card update date'),
                    image: Joi.string()
                        .optional()
                        .description('Card image'),
                    id_creator: Joi.string()
                        .required()
                        .description('Card id_creator id'),
                    id_game: Joi.string()
                        .guid()
                        .required()
                        .description("Games card's id")
                }).allow(null),
                failAction: async(request, h, err) => {
                    console.error(err);
                    throw err;
                }
            },
            payload: {
                multipart: true
            }
        }
    },
    {
        method: 'DELETE',
        path: '/api/deleteCard',
        options: {
            auth: false,
            handler: (request, h) => handlers.deleteCard(request, h),
            description: 'Delete card in database',
            tags: ['api'],
            validate: {
                query: Joi.object({
                    id: Joi.string()
                        .guid()
                        .required()
                        .description('Card id')
                })
            }
        }
    }
];