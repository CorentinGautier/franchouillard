const apiHealthcheck = {
    method: 'GET',
    path: '/api/status',
    options: {
        auth: false,
        handler: () => 'OK',
        description: 'Get the API status',
        tags: ['api'],
        validate: {}
    }
};

module.exports = server => {
    // GET /api/status
    server.route(apiHealthcheck);

    // /api/card
    server.route(require('./card'));

    //  /api/user
    server.route(require('./user'));

    // /api/game
    server.route(require('./game'));

    // /api/type
    server.route(require('./type'));
};