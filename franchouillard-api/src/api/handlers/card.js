const Jwt = require('jsonwebtoken');
const Boom = require('@hapi/boom');

const logger = require('../../../logger').getLogger('SERVER');
const models = require('../models/card.js');


exports.getCardsByGameId = async(request, h) => {
    try {
        //modfication =
        return await models.getCardsByGameId(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }

        return Boom.badRequest(err);
    }
};

exports.getCardById = async(request, h) => {
    try {
        return await models.getCardById(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }
        return Boom.badRequest(err);
    }
};

exports.insertCard = async(request, h) => {
    try {
        return await models.insertCard(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }
        return Boom.badRequest(err);
    }
};

exports.updateCard = async(request, h) => {
    try {
        return await models.updateCard(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }
        return Boom.badRequest(err);
    }
};

exports.deleteCard = async(request, h) => {
    try {
        return await models.deleteCard(request)
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }

        return Boom.badRequest(err);
    }
}