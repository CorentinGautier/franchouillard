const Jwt = require('jsonwebtoken');
const Boom = require('@hapi/boom');
const R = require('ramda');

const logger = require('../../../logger').getLogger('SERVER');
const models = require('../models/user.js');

exports.login = async(request, h) => {
    try {
        return await models.login(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }

        return Boom.badRequest(err);
    }
};
exports.addgameToUser = async(request, h) => {
    try {
        return await models.addgameToUser(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }

        return Boom.badRequest(err);
    }
};
exports.getUsers = async(request, h) => {
    try {
        return await models.getUsers(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }

        return Boom.badRequest(err);
    }
};
exports.getUserById = async(request, h) => {
    try {
        return await models.getUserById(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }

        return Boom.badRequest(err);
    }
};

exports.createUser = async(request, h) => {
    try {
        return await models.createUser(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }
        return Boom.badRequest(err);
    }
};

exports.deleteUser = async(request, h) => {
    try {
        return await models.deleteUser(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }

        return Boom.badRequest(err);
    }
};
exports.updateUser = async(request, h) => {
    try {
        return await models.deleteUser(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }

        return Boom.badRequest(err);
    }
};