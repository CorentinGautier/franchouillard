const Boom = require('@hapi/boom');

const logger = require('../../../logger').getLogger('SERVER');
const models = require('../models/type.js');

exports.getTypes = async(request, h) => {
    try {
        return await models.getTypes(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }
        return Boom.badRequest(err);
    }
};

exports.getTypeById = async(request, h) => {
    try {
        return await models.getTypeById(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }
        return Boom.badRequest(err);
    }
};
exports.insertType = async(request, h) => {
    try {
        return await models.insertType(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }
        return Boom.badRequest(err);
    }
};

exports.updateType = async(request, h) => {
    try {
        return await models.updateType(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }
        return Boom.badRequest(err);
    }
};

exports.deleteType = async(request, h) => {
    try {
        return await models.deleteType(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }

        return Boom.badRequest(err);
    }
};