const Jwt = require('jsonwebtoken');
const Boom = require('@hapi/boom');

const logger = require('../../../logger').getLogger('SERVER');
const models = require('../models/game.js');


//couche métier
exports.getGameById = async(request, h) => {
    try {
        return await models.getGameById(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }
        return Boom.badRequest(err);
    }
};


exports.getGames = async(request, h) => {
    try {
        return await models.getGames(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }
        return Boom.badRequest(err);
    }
};
exports.updateGameMinParticipant = async(request, h) => {
    try {
        return await models.updateGameMinParticipant(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }
        return Boom.badRequest(err);
    }
};

exports.insertGame = async(request, h) => {
    try {
        return await models.insertGame(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }
        return Boom.badRequest(err);
    }
};

exports.updateGame = async(request, h) => {
    try {
        return await models.updateGame(request);
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }
        return Boom.badRequest(err);
    }
};

exports.deleteGame = async(request, h) => {
    try {
        return await models.deleteGame(request)
    } catch (err) {
        if (err.isBoom) {
            logger.error(err.message);
            return err;
        }

        return Boom.badRequest(err);
    }
}