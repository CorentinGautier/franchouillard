const Hapi = require('@hapi/hapi');
const R = require('ramda');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('../package.json');
const HapiJwt = require('hapi-auth-jwt2');

const config = require('../config');
const logger = require('../logger').getLogger('SERVER');

const dbService = require('./database');

// conf api

const server = new Hapi.Server({
    host: config.api.host,
    port: config.api.port,
    routes: {
        cors: {
            origin: ['*'],
            credentials: false,
        },
    }
});

const swaggerOptions = {
    info: {
        title: 'API Documentation',
        version: Pack.version
    },
    documentationPage: process.env.NODE_ENV !== 'production'
};

const validateToken = async token => { //genère le token
    const db = dbService.getDb();

    try {
        const result = await db.user.findOne({ us_id: token.us_id });
        return R.isNil(result) ? { isValid: false } : { isValid: true };
    } catch (err) {
        logger.error(err);
        return err;
    }
};

const start = async() => { //definie l hapi jwt
    // Register plugins
    await server.register([
        HapiJwt,
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ]);

    await dbService.initDatabase();

    await server.start();

    // Import routes
    require('./api/routes')(server);

    server.auth.strategy('jwt', 'jwt', {
        key: config.jwt,
        verifyOptions: { algorithms: ['HS256'] },
        validate: validateToken
    });

    server.auth.default('jwt');
};

const stop = () => server.stop();

module.exports = {
    server,
    start,
    stop
};