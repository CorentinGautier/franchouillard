const logger = require('../logger').getLogger('SERVER');

const massive = require('massive');
const config = require('../config');

let db;

exports.initDatabase = async () => {
  db = await massive({
    host: config.postgresql.host,
    port: config.postgresql.port,
    database: config.postgresql.database,
    user: config.postgresql.user,
    password: config.postgresql.password,
    ssl: false,
    poolSize: 10
  });

  logger.info('Database connected');

  return db;
};

exports.getDb = () => db;
