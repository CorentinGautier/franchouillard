import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { handleError, httpOptions } from './service.service';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  APIPATH = environment.APIPATH;

  constructor(private httpClient: HttpClient) {}

  public getGames(): Observable<Object[]> {
    return this.httpClient
      .get<Object[]>(this.APIPATH + 'games', httpOptions)
      .pipe(catchError(handleError<Object[]>('getGames', [])));
  }

  public getGameById(gameId): Observable<object> {
    return this.httpClient
      .get<object>(this.APIPATH + 'gameById?id=' + gameId, httpOptions)
      .pipe(catchError(handleError<object>('gameById', [])));
  }

  public updateGameMinParticipant(gameId, minParticipant) {
    let val = {
      id: gameId,
      minParticipant: minParticipant
    };
    this.httpClient.put(this.APIPATH + 'updateGameMinParticipant', val, httpOptions).subscribe(
      success => {
        return true;
      },
      error => {
        console.error('There was an error!', error);
        return false;
      }
    );
  }

  public addGame(newGame) {
    this.httpClient.post(this.APIPATH + 'addGame', newGame, httpOptions).subscribe(
      success => {
        return true;
      },
      error => {
        console.error('There was an error!', error);
        return false;
      }
    );
  }

  public updateGame(updateGame) {
    this.httpClient.put(this.APIPATH + 'updateGame', updateGame, httpOptions).subscribe(
      success => {
        return true;
      },
      error => {
        console.error('There was an error!', error);
        return false;
      }
    );
  }

  deleteGame(id: any) {
    this.httpClient.delete(this.APIPATH + 'deleteGame?id=' + id, httpOptions).subscribe(
      success => {
        return true;
      },
      error => {
        console.error('There was an error!', error);
        return false;
      }
    );
  }
}
