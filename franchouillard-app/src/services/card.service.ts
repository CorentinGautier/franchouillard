import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { handleError, httpOptions } from './service.service';
import { Observable } from 'rxjs';
import { MessageService } from 'primeng/api';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CardService {
  APIPATH = environment.APIPATH;

  constructor(private httpClient: HttpClient, private messageService: MessageService) {}

  public getCardsByGameId(gameId): Observable<Object[]> {
    return this.httpClient
      .get<Object[]>(this.APIPATH + 'cardsbyGameId?id_game=' + gameId, httpOptions)
      .pipe(catchError(handleError<Object[]>('cardsbyGameId', [])));
  }

  public getCardsById(cardId): void {
    this.httpClient.get(this.APIPATH + 'cardsbyId?id=' + cardId, httpOptions).subscribe(
      success => {
        return success;
      },
      error => {
        console.error('There was an error!', error);
        return error;
      }
    );
  }

  public addCard(newCard) {
    this.httpClient.post(this.APIPATH + 'addCard', newCard, httpOptions).subscribe(
      success => {
        return true;
      },
      error => {
        console.error('There was an error!', error);
        return false;
      }
    );
  }

  public updateCard(updateCard) {
    this.httpClient.put(this.APIPATH + 'updateCard', updateCard, httpOptions).subscribe(
      success => {
        return true;
      },
      error => {
        console.error('There was an error!', error);
        return false;
      }
    );
  }

  deleteCard(id: any) {
    this.httpClient.delete(this.APIPATH + 'deleteCard?id=' + id, httpOptions).subscribe(
      success => {
        return true;
      },
      error => {
        console.error('There was an error!', error);
        return false;
      }
    );
  }
}
