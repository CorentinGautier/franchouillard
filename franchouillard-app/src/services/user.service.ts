import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { Observable } from 'rxjs-compat';
import { catchError } from 'rxjs/operators';
import { handleError, httpOptions } from './service.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  APIPATH = environment.APIPATH;

  constructor(private httpClient: HttpClient, private router: Router) {}

  public login(user): void {
    this.httpClient
      .get(
        this.APIPATH + 'users/login?username=' + user.username + '&email=' + user.email,
        httpOptions
      )
      .subscribe(
        success => {
          localStorage.setItem('user', JSON.stringify(success));
          localStorage.setItem('visitor', 'false');

          this.router.navigate(['app/list']);
          return success;
        },
        error => {
          console.error('There was an error!', error);
          return error;
        }
      );
  }

  public getUsersById(userId): Observable<Object[]> {
    return this.httpClient
      .get<Object[]>(this.APIPATH + 'user?id=' + userId, httpOptions)
      .pipe(catchError(handleError<Object[]>('user', [])));
  }

  public getUsers(): Observable<Object[]> {
    return this.httpClient
      .get<Object[]>(this.APIPATH + 'users', httpOptions)
      .pipe(catchError(handleError<Object[]>('users', [])));
  }

  public addUser(newUser) {
    this.httpClient.post(this.APIPATH + 'users', newUser, httpOptions).subscribe(
      success => {
        localStorage.setItem('user', JSON.stringify(success));
        localStorage.setItem('visitor', 'false');

        this.router.navigate(['app/list']);
        return success;
      },
      error => {
        console.error('There was an error!', error);
        return false;
      }
    );
  }

  public updateUser(updateUser) {
    this.httpClient.put(this.APIPATH + 'users', updateUser, httpOptions).subscribe(
      success => {
        return true;
      },
      error => {
        console.error('There was an error!', error);
        return false;
      }
    );
  }

  deleteUser(id: any) {
    this.httpClient.delete(this.APIPATH + 'users/' + id, httpOptions).subscribe(
      success => {
        return true;
      },
      error => {
        console.error('There was an error!', error);
        return false;
      }
    );
  }
}
