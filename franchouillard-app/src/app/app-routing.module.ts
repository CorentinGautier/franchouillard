import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';

// Franchouillard
import { FinalGameComponent } from './franchouillards/final-game/final-game.component';
import { GameComponent } from './franchouillards/game/game.component';
import { GenerateNewGameComponent } from './franchouillards/generate-update-game/generate-update-game.component';
import { ListComponent } from './franchouillards/list/list.component';
import { MenuComponent } from './menu/menu.component';

//Point Beauf
import { AddPointComponent } from './pointBeauf/addPoint/addPoint.component';
import { LeaderBoardComponent } from './pointBeauf/leaderBoard/leaderBoard.component';

const routes: Routes = [
  //Global
  { path: '', redirectTo: 'app/auth', pathMatch: 'full' },
  { path: 'app/auth', component: AuthComponent },
  { path: 'app/menu', component: MenuComponent },

  //Franchouillard
  { path: 'app/list', component: ListComponent },
  { path: 'app/game/:id', component: GameComponent },
  { path: 'app/finalGame/:id', component: FinalGameComponent },
  { path: 'app/generateNewGame', component: GenerateNewGameComponent },

  //Point beauf
  { path: 'app/pointBeauf/leaderBoard', component: LeaderBoardComponent },
  { path: 'app/pointBeauf/addPoint', component: AddPointComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
