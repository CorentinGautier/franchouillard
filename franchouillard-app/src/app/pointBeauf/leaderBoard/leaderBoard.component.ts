import { Router } from '@angular/router';
import { GameService } from 'src/services/game.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-leaderBoard',
  templateUrl: './leaderBoard.component.html',
  styleUrls: ['./leaderBoard.component.css']
})
export class LeaderBoardComponent implements OnInit {
  public faSearch = faSearch;

  public selectedGame: any;
  public gamesDisplayed: any;
  public games: any;
  public searchForm: any;
  public visitor = localStorage.getItem('visitor');

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private gameService: GameService
  ) {
    this.searchForm = this.formBuilder.group({
      searchValue: ['', [Validators.maxLength(255)]]
    });
  }

  ngOnInit(): void {
    let user = localStorage.getItem('user');
    let googleUser = localStorage.getItem('googleUser');
    if (!this.visitor) {
      this.router.navigate(['/app/auth']);
    } else if ((!user && this.visitor === 'false') || (!googleUser && this.visitor === 'false')) {
      this.router.navigate(['/app/auth']);
    } else if (user && googleUser) {
      this.visitor = 'false';
    } else if (!user && !googleUser && this.visitor === 'true') {
      this.visitor = 'true';
    }
    this.getGames();
  }

  public getGames() {
    this.gameService.getGames().subscribe(games => {
      this.games = games;
      this.gamesDisplayed = games;
    });
  }

  public onSelect(game) {
    this.selectedGame = game;
  }

  public searchBar() {
    if (this.searchForm.value.searchValue != null) {
      this.gamesDisplayed = [];
      for (let game of this.games) {
        let title = game.name.toLowerCase();
        let description = game.description.toLowerCase();
        if (title.includes(this.searchForm.value.searchValue) === true) {
          this.gamesDisplayed.push(game);
        } else if (description.search(this.searchForm.value.searchValue) === true) {
          this.gamesDisplayed.push(game);
        }
      }
    }
  }
}
