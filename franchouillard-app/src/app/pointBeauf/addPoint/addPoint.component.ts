import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-addPoint',
  templateUrl: './addPoint.component.html',
  styleUrls: ['./addPoint.component.css']
})
export class AddPointComponent implements OnInit {
  constructor(private formBuilder: FormBuilder, private router: Router) {}

  ngOnInit(): void {}
}
