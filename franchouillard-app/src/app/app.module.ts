import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//Composant primeNg
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { AccordionModule } from 'primeng/accordion';
import { InputSwitchModule } from 'primeng/inputswitch';

//global
import { AuthComponent } from '../app/auth/auth.component';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';

//Franchouillard
import { CardComponent } from './franchouillards/card/card.component';
import { GameComponent } from './franchouillards/game/game.component';
import { ParticipantComponent } from './franchouillards/participant/participant.component';
import { AddEditCardComponent } from './franchouillards/add-edit-card/add-edit-card.component';
import { GenerateNewGameComponent } from './franchouillards/generate-update-game/generate-update-game.component';
import { FinalGameComponent } from './franchouillards/final-game/final-game.component';
import { AddEditGameComponent } from './franchouillards/add-edit-game/add-edit-game.component';
import { ButtonHelpComponent } from './franchouillards/button/button-help/button-help.component';
import { ButtonBackComponent } from './franchouillards/button/button-back/button-back.component';
import { ListComponent } from './franchouillards/list/list.component';

//Point Beauf
import { LeaderBoardComponent } from './pointBeauf/leaderBoard/leaderBoard.component';
import { AddPointComponent } from './pointBeauf/addPoint/addPoint.component';

@NgModule({
  declarations: [
    //Globale
    AppComponent,
    AuthComponent,
    MenuComponent,

    //Franchouillard
    CardComponent,
    ParticipantComponent,
    ListComponent,
    GameComponent,
    FinalGameComponent,
    AddEditCardComponent,
    AddEditGameComponent,
    GenerateNewGameComponent,
    ButtonHelpComponent,
    ButtonBackComponent,

    // Point beauf
    AddPointComponent,
    LeaderBoardComponent
  ],
  imports: [
    BrowserAnimationsModule,
    CardModule,
    ButtonModule,
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    ToastModule,
    TableModule,
    AppRoutingModule,
    ToastModule,
    BrowserModule,
    DialogModule,
    InputSwitchModule,
    TableModule,
    HttpClientModule,
    FormsModule,
    AccordionModule,
    FontAwesomeModule,
    BrowserModule,
    BrowserModule
  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule {}
