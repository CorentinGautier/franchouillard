import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/services/user.service';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  public user;
  constructor(private router: Router, private userService: UserService, private zone: NgZone) {}

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
  }
}
