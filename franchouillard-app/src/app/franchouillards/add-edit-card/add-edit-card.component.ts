import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CardService } from 'src/services/card.service';
import { GameService } from 'src/services/game.service';

import * as uuid from 'uuid';

@Component({
  selector: 'app-add-edit-card',
  templateUrl: './add-edit-card.component.html',
  styleUrls: ['./add-edit-card.component.css']
})
export class AddEditCardComponent implements OnInit, OnChanges {
  @Input() updateCard: boolean;
  @Input() createCard: boolean;
  @Input() selectedGame: any;
  @Input() selectedCard: any;
  @Output() onRefresh = new EventEmitter();
  display: boolean;

  public cardForm: any;
  public title = 'Ajouter une carte';
  public PARTICIP_CHARAC = '##';
  public cardPicture: string = null;
  user: any;

  constructor(
    private formBuilder: FormBuilder,
    private cardService: CardService,
    private gameService: GameService
  ) {
    this.cardForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(255)]],
      type: ['', [Validators.required, Validators.maxLength(255)]],
      text1: ['', [Validators.required, Validators.maxLength(255)]],
      text2: ['', [Validators.required, Validators.maxLength(255)]]
    });
  }

  ngOnChanges(): void {
    if (this.updateCard || this.createCard) {
      this.display = true;
    }
    if (this.createCard) {
      this.title = 'Ajouter une carte';
    } else {
      this.title = 'Modifier la carte';
    }
    if (this.selectedCard) {
      this.cardForm = this.formBuilder.group({
        name: this.selectedCard.name,
        type: this.selectedCard.type,
        text1: this.selectedCard.text1,
        text2: this.selectedCard.text2
      });
    }
  }
  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user'));
    if (this.createCard) {
      this.title = 'Ajouter une carte';
    } else {
      this.title = 'Modifier la carte';
    }
  }

  public validate() {
    if (this.cardForm.value.text1) {
      let minParticipant = 0;
      minParticipant = this.countCaractInString(this.cardForm.value.text1);
      if (minParticipant < this.countCaractInString(this.cardForm.value.text2)) {
        minParticipant = this.countCaractInString(this.cardForm.value.text2);
      }
      if (minParticipant < 2) {
        minParticipant = 2;
      }
      if (this.selectedGame.minParticipant < minParticipant) {
        this.gameService.updateGameMinParticipant(this.selectedGame.id, minParticipant);
      }
      if (this.createCard) {
        let finalCard = {
          id: uuid.v4(),
          name: this.cardForm.value.name ? this.cardForm.value.name : ' ',
          text1: this.cardForm.value.text1,
          text2: this.cardForm.value.text2 ? this.cardForm.value.text2 : ' ',
          date_create: Date.now(),
          date_update: Date.now(),
          type: this.cardForm.value.type ? this.cardForm.value.type : ' ',
          image: this.cardForm.value.image ? this.cardForm.value.image : ' ',
          id_game: this.selectedGame.id,
          id_creator: this.user.id
        };
        this.cardService.addCard(finalCard);
        this.onRefresh.emit(this.selectedGame.id);
        this.display = false;
      } else {
        let finalCard = {
          id: this.selectedCard.id,
          name: this.cardForm.value.name ? this.cardForm.value.name : ' ',
          text1: this.cardForm.value.text1,
          text2: this.cardForm.value.text2 ? this.cardForm.value.text2 : ' ',
          date_create: this.selectedCard.date_create,
          date_update: Date.now(),
          type: this.cardForm.value.type ? this.cardForm.value.type : ' ',
          image: this.cardForm.value.image ? this.cardForm.value.image : ' ',
          id_game: this.selectedGame.id,
          id_creator: this.user.id
        };
        this.cardService.updateCard(finalCard);
        this.onRefresh.emit(this.selectedGame.id);
        this.display = false;
      }
      this.cardForm.reset();
    }
  }

  public deleteCard() {
    this.cardService.deleteCard(this.selectedCard.id);
    this.display = false;
    this.onRefresh.emit(this.selectedGame.id);
  }

  private countCaractInString(str) {
    if (str) {
      let count = 0;
      let pos = str.indexOf(this.PARTICIP_CHARAC);
      while (pos != -1) {
        count++;
        pos = str.indexOf(this.PARTICIP_CHARAC, pos + 1);
      }
      return count;
    }
  }
}
