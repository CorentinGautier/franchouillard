import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CardService } from 'src/services/card.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input() idSelectedGame: any;
  @Input() participants: [];
  public cards;
  public duplicateCards;
  public actualCard: any;
  public index;
  public PARTICIP_CHARAC = '##';
  public val = 1;
  public typeCard = ['A', 'Q', 'K', 'V', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
  valLetter: string;

  constructor(private router: Router, private cardService: CardService) {}

  ngOnInit() {
    this.val = this.entierAleatoire(1, 4);
    this.valLetter = this.typeCard[this.entierAleatoire(1, this.typeCard.length - 1)];
    this.actualCard = null;
    this.index = null;
    this.getCards();
  }

  public getCards() {
    this.cardService.getCardsByGameId(this.idSelectedGame).subscribe(cards => {
      this.cards = cards;
      this.duplicateCards = this.cards;
      this.play();
    });
  }

  public randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  public play() {
    if (this.duplicateCards) {
      this.checkFinal();
      this.index = this.randomInt(0, this.duplicateCards.length - 1);
      this.actualCard = this.duplicateCards[this.index];
      if (this.actualCard) {
        this.actualCard.text1 = this.checkAvancedCard(this.actualCard.text1);
        this.actualCard.text2 = this.checkAvancedCard(this.actualCard.text2);
        this.checkFinal();
      }
    }
  }

  public nextCard() {
    this.val = this.entierAleatoire(1, 4);
    this.valLetter = this.typeCard[this.entierAleatoire(1, this.typeCard.length - 1)];
    this.checkFinal();
    this.duplicateCards.splice(this.index, 1);
    this.play();
  }

  public checkFinal() {
    if (this.duplicateCards.length === 0) {
      this.router.navigate(['/app/finalGame', this.idSelectedGame]);
    }
  }

  public checkAvancedCard(str) {
    this.participants = JSON.parse(localStorage.getItem('participants'));
    let participantDuplicated = JSON.parse(localStorage.getItem('participants'));
    let participantSelected = [];
    let stringFinal: string = str;
    let nbCarac = this.countCaractInString(stringFinal);
    for (let i = 0; i < nbCarac; i++) {
      let indexDeletedParticipant = this.randomInt(0, participantDuplicated.length - 1);
      participantSelected.push(participantDuplicated[indexDeletedParticipant]);
      participantDuplicated.splice(indexDeletedParticipant, 1);
    }
    for (let particip of participantSelected) {
      stringFinal = stringFinal.replace(/##/, particip.nom);
    }
    participantDuplicated = this.participants;
    participantSelected = [];
    participantDuplicated = this.participants;
    participantSelected = [];
    return stringFinal;
  }

  private countCaractInString(str) {
    if (str) {
      let count = 0;
      let pos = str.indexOf(this.PARTICIP_CHARAC);
      while (pos != -1) {
        count++;
        pos = str.indexOf(this.PARTICIP_CHARAC, pos + 1);
      }
      return count;
    }
  }

  private entierAleatoire(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}
