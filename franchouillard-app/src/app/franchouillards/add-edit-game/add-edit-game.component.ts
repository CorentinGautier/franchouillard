import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { GameService } from 'src/services/game.service';
import * as uuid from 'uuid';

@Component({
  selector: 'app-add-edit-game',
  templateUrl: './add-edit-game.component.html',
  styleUrls: ['./add-edit-game.component.css']
})
export class AddEditGameComponent implements OnInit, OnChanges {
  checked: boolean = false;
  @Input() updateGame: boolean;
  @Input() createGame: boolean;
  @Input() selectedGame: any;
  @Output() onRefresh = new EventEmitter();
  @Output() onRefreshAll = new EventEmitter();
  display: boolean;

  public gameForm: any;
  public title = 'Ajouter un jeux';
  date = new Date();
  user: any;

  constructor(private formBuilder: FormBuilder, private gameService: GameService) {
    this.gameForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(255)]],
      description: ['', [Validators.required, Validators.maxLength(255)]],
      publicate: [this.checked]
    });
  }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.date = new Date(Date.now());
    if (this.createGame) {
      this.title = 'Ajouter un jeux';
    } else {
      this.title = 'Modifier le jeux';
    }
  }
  ngOnChanges(): void {
    if (this.updateGame || this.createGame) {
      this.display = true;
    }
    if (this.updateGame) {
      this.gameForm = this.formBuilder.group({
        name: this.selectedGame.name,
        description: this.selectedGame.description,
        publicate: this.selectedGame.publicate
      });
    }
    if (this.createGame) {
      this.title = 'Ajouter un jeux';
    } else {
      this.title = 'Modifier le jeux';
    }
  }
  public validate() {
    if ((this.gameForm.value.name, this.gameForm.value.description)) {
      if (this.createGame) {
        let finalGame = {
          id: uuid.v4(),
          name: this.gameForm.value.name,
          description: this.gameForm.value.description,
          image: 'image',
          publicate: this.gameForm.value.publicate ? this.gameForm.value.publicate : false,
          date_create: this.date.toISOString(),
          date_update: this.date.toISOString(),
          date_publicate: this.date.toISOString(),
          id_creator: this.user.id,
          minParticipant: 2
        };
        this.gameService.addGame(finalGame);
        this.onRefreshAll.emit();
        this.display = false;
        this.gameForm.reset();
      } else {
        let finalGame = {
          id: this.selectedGame.id,
          name: this.gameForm.value.name,
          description: this.gameForm.value.description,
          image: this.selectedGame.image,
          publicate: this.gameForm.value.publicate ? this.gameForm.value.publicate : false,
          date_create: this.selectedGame.date_create,
          date_update: this.date.toISOString(),
          date_publicate: this.selectedGame.date_publicate
            ? this.selectedGame.date_publicate
            : this.gameForm.value.publicate
            ? this.date.toISOString()
            : '',
          id_creator: this.user.id,
          minParticipant: 2
        };
        this.gameService.updateGame(finalGame);
        this.display = false;
        this.onRefreshAll.emit();

        this.gameForm.reset();
      }
      this.display = false;
    }
  }
  public deleteGame() {
    this.gameService.deleteGame(this.selectedGame.id);
    this.display = false;
    this.onRefreshAll.emit();
  }
}
