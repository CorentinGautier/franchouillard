import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  public games: any;
  public viewParticipant = false;
  public viewCards = false;
  public participants: any[];
  public idSelectedGame: any;
  public game: any;
  public visitor = localStorage.getItem('visitor');

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    let user = localStorage.getItem('user');
    let googleUser = localStorage.getItem('googleUser');
    if (!this.visitor) {
      this.router.navigate(['/app/auth']);
    } else if ((!user && this.visitor === 'false') || (!googleUser && this.visitor === 'false')) {
      this.router.navigate(['/app/auth']);
    } else if (user && googleUser) {
      this.visitor = 'false';
    } else if (!user && !googleUser && this.visitor === 'true') {
      this.visitor = 'true';
    }
    this.activatedRoute.params.subscribe(params => {
      this.idSelectedGame = params['id'];
    });
    if (!this.participants) {
      this.displayParticipant();
    } else {
      this.displayCards();
    }
  }

  public onReceiveParticipant(event) {
    this.participants = event;
    this.displayCards();
  }

  public displayParticipant() {
    this.viewParticipant = true;
    this.viewCards = false;
  }

  public displayCards() {
    this.viewParticipant = false;
    this.viewCards = true;
  }
}
