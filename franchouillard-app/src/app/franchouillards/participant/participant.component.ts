import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { GameService } from 'src/services/game.service';

@Component({
  selector: 'app-participant',
  templateUrl: './participant.component.html',
  styleUrls: ['./participant.component.css']
})
export class ParticipantComponent implements OnInit {
  @Output() onValidateParticipant: EventEmitter<any> = new EventEmitter();
  @Input() idSelectedGame;
  public game;
  public participants: any;
  public participantForm: any;
  public error = null;

  constructor(private formBuilder: FormBuilder, private gameService: GameService) {
    this.participantForm = this.formBuilder.group({
      name: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.error = null;
    this.getGameInfo();
    this.participants = [];
    if (localStorage.getItem('participants')) {
      this.participants = JSON.parse(localStorage.getItem('participants'));
    }
  }

  public getGameInfo() {
    this.gameService.getGameById(this.idSelectedGame).subscribe(game => {
      this.game = game;
    });
  }

  public addParticipant() {
    if (this.participantForm.value.name) {
      this.participants.push({
        nom: this.participantForm.value.name
      });
      this.error = null;
      this.participantForm.reset();
    }
  }

  public deleteParticipant(val) {
    let index = 0;
    for (let participant of this.participants) {
      if (participant.nom === val) {
        this.participants.splice(index, 1);
        this.error = null;
      }
      index++;
    }
  }

  public validate() {
    if (this.participants && this.participants.length >= this.game.minParticipant) {
      localStorage['participants'] = JSON.stringify(this.participants);
      this.onValidateParticipant.emit(this.participants);
      this.error = null;
    } else {
      this.error =
        'Il faut au minimun de ' + this.game.minParticipant + ' participants pour jouer à ce jeux';
    }
  }
}
