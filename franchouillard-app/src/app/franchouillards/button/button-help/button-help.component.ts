import { Component, Input, OnInit } from '@angular/core';
import { faInfo } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-button-help',
  templateUrl: './button-help.component.html',
  styleUrls: ['./button-help.component.scss']
})
export class ButtonHelpComponent implements OnInit {
  @Input() text = '';
  public faInfo = faInfo;

  constructor() {}

  ngOnInit(): void {}
}
