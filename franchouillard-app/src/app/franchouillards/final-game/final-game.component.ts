import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-final-game',
  templateUrl: './final-game.component.html',
  styleUrls: ['./final-game.component.css']
})
export class FinalGameComponent implements OnInit {
  @Input() actualGameId: number;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.actualGameId = params['id'];
    });
  }
}
