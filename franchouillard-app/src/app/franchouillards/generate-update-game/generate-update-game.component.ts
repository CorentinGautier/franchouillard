import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CardService } from 'src/services/card.service';
import { GameService } from 'src/services/game.service';
import { UserService } from 'src/services/user.service';
import { AddEditGameComponent } from '../add-edit-game/add-edit-game.component';

@Component({
  selector: 'app-generate-update-game',
  templateUrl: './generate-update-game.component.html',
  styleUrls: ['./generate-update-game.component.css']
})
export class GenerateNewGameComponent implements OnInit {
  public cards: any;
  public games: any;
  public displayAddUpdateCards: boolean = false;
  public displayAddUpdateGame: boolean = false;
  public selectedGame: any;
  public selectedAuth = null;
  public selectedCard: any;

  public cols: any[];

  public createCard: boolean = false;
  public updateCard: boolean = false;
  public createGame: boolean = false;
  public updateGame: boolean = false;

  @ViewChild(AddEditGameComponent, { static: false }) childRef: AddEditGameComponent;
  public visitor = localStorage.getItem('visitor');

  constructor(
    private cardService: CardService,
    private router: Router,
    private gameService: GameService,
    private userService: UserService
  ) {}

  ngOnInit() {
    let user = localStorage.getItem('user');
    let googleUser = localStorage.getItem('googleUser');
    if (!this.visitor) {
      this.router.navigate(['/app/auth']);
    } else if ((!user && this.visitor === 'false') || (!googleUser && this.visitor === 'false')) {
      this.router.navigate(['/app/auth']);
    } else if (user && googleUser) {
      this.visitor = 'false';
    } else if (!user && !googleUser && this.visitor === 'true') {
      this.visitor = 'true';
    }
    this.getGames();
    this.cols = [
      { field: 'name', header: 'Nom' },
      { field: 'text1', header: 'Texte principal' },
      { field: 'creator', header: 'Utilisateur' }
    ];
  }

  ngOnChanges(): void {
    let user = localStorage.getItem('user');
    let googleUser = localStorage.getItem('googleUser');
    let visitor = localStorage.getItem('visitor');
    if (!visitor) {
      this.router.navigate(['/app/auth']);
    } else if ((!user && visitor === 'false') || (!googleUser && visitor === 'false')) {
      this.router.navigate(['/app/auth']);
    }
  }
  getGames() {
    this.gameService.getGames().subscribe(games => {
      this.games = games;
    });
  }

  public refresh(event?: null) {
    this.closeAllDialog();
    this.getGames();
    this.getGameCards(event);
    event ? this.openSelectedGame(event) : null;
  }

  public refreshGame(event?: null) {
    this.closeAllDialog();
    this.getGames();
    event ? this.getGameCards(event) : null;
    event ? this.openSelectedGame(event) : null;
  }

  public refreshAll() {
    window.location.reload();
  }
  public openSelectedGame(id) {
    let i = 0;
    for (let game of this.games) {
      if (game.id === id) {
        this.selectedAuth = i;
        this.getGameCards(this.games[this.selectedAuth].id);
      }
      i++;
    }
  }
  public onSelectGame(game) {
    this.selectedGame = game;
    this.getGameCards(game.id);
  }

  public onSelectCard(card) {
    this.selectedCard = card;
    this.showDialogUpdateCard();
  }

  private getGameCards(gameId) {
    if (gameId) {
      this.cardService.getCardsByGameId(gameId).subscribe(cards => {
        this.cards = cards;
        for (let card of this.cards) {
          this.userService.getUsersById(card.id_creator).subscribe(sucess => {
            if (sucess) {
              let user: any = sucess;
              card.creator = user.username;
            }
          });
        }
      });
      this.gameService.getGameById(gameId).subscribe(game => {
        this.selectedGame = game;
      });
    }
  }

  showDialogAddCard() {
    this.selectedCard = null;
    this.closeAllDialog();
    this.displayAddUpdateCards = true;
    this.displayAddUpdateGame = false;
    this.createCard = true;
    this.updateCard = false;
  }

  showDialogUpdateCard() {
    this.closeAllDialog();
    this.displayAddUpdateCards = true;
    this.displayAddUpdateGame = false;
    this.createCard = false;
    this.updateCard = true;
  }

  showDialogAddGame() {
    this.closeAllDialog();
    this.displayAddUpdateGame = true;
    this.displayAddUpdateCards = false;
    this.createGame = true;
    this.updateGame = false;
  }
  showDialogUpdateGame() {
    this.closeAllDialog();
    this.displayAddUpdateGame = true;
    this.displayAddUpdateCards = false;
    this.createGame = false;
    this.updateGame = true;
  }

  public closeAllDialog() {
    this.displayAddUpdateGame = false;
    this.displayAddUpdateGame = false;
    this.displayAddUpdateCards = false;
    this.displayAddUpdateCards = false;
    this.createGame = false;
    this.updateGame = false;
  }

  public checkSelected(id) {
    if (id && this.selectedGame) {
      if (this.selectedGame.id === id) {
        return true;
      } else {
        return false;
      }
    }
  }
}
