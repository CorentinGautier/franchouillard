import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/services/user.service';
@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  public auth2: any;

  @ViewChild('loginRef', { static: true }) loginElement: ElementRef;
  public users: any;

  constructor(private router: Router, private userService: UserService, private zone: NgZone) {}

  ngOnInit() {
    this.googleSDK();
  }

  prepareLoginButton() {
    this.auth2.attachClickHandler(
      this.loginElement.nativeElement,
      {},
      googleUser => {
        let profile = googleUser.getBasicProfile();
        // console.log('Token || ' + googleUser.getAuthResponse().id_token);
        // console.log('ID: ' + profile.getId());
        // console.log('Name: ' + profile.getName());
        // console.log('Image URL: ' + profile.getImageUrl());
        // console.log('Email: ' + profile.getEmail());
        localStorage.setItem('googleUser', JSON.stringify(profile));
        this.connexion(profile);
        return;
      },
      error => {
        alert(JSON.stringify(error, undefined, 2));
      }
    );
  }
  googleSDK() {
    window['googleSDKLoaded'] = () => {
      window['gapi'].load('auth2', () => {
        this.auth2 = window['gapi'].auth2.init({
          client_id: '999622604293-54bgo6lo3cnrtpft076hkqhr2r94s506.apps.googleusercontent.com',
          cookie_policy: 'single_host_origin',
          scope: 'profile email'
        });
        this.prepareLoginButton();
      });
    };

    (function (d, s, id) {
      var js,
        fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = 'https://apis.google.com/js/platform.js?onload=googleSDKLoaded';
      fjs.parentNode.insertBefore(js, fjs);
    })(document, 'script', 'google-jssdk');
  }

  public logVisitor() {
    localStorage.removeItem('user');
    localStorage.removeItem('googleUser');
    localStorage.setItem('visitor', 'true');
    this.router.navigate(['/app/menu']);
  }

  public connexion(profile) {
    let machin = false;
    this.userService.getUsers().subscribe(users => {
      this.users = users;

      if (this.users[0]) {
        for (let user of this.users) {
          if (user.email === profile.getEmail() && user.username === profile.getName()) {
            this.userService.login(user);
            machin = false;
            this.zone.run(() => {
              this.router.navigate(['app/menu']);
            });

            return;
          } else {
            machin = true;
          }
        }
        if (machin) {
          this.register(profile);
        }
      } else {
        this.register(profile);
      }
    });
  }

  public register(profile) {
    if (profile) {
      let user = {
        username: profile.getName(),
        email: profile.getEmail(),
        password: profile.getId()
      };
      this.userService.addUser(user);
      this.zone.run(() => {
        this.router.navigate(['app/menu']);
      });
    }
  }

  public logOut() {
    localStorage.removeItem('user');
    localStorage.removeItem('googleUser');
  }
}
